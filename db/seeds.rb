todos = Task.create([
  {title: "Walk the dog", description: "It's time to walk the dog again.", completed: false},
  {title: "Go shopping", description: "Go to the store and buy all the groceries.", completed: false},
  {title: "Wash the Car", description: "The car is filthy", completed: false},
  {title: "Clean my room", description: "There is stuff everywhere", completed: false},
  {title: "Do the Laundry", description: "Don't forget the socks!", completed: false},
  {title: "Make a sandwich", description: "I will be hungry if I don't eat a sandwich", completed: true},
  {title: "Walk the Cat", description: "Take Fido around the block.", completed: true}
   ])
