# README

* Extract and cd to project directory

* bundle install

* rake db:create
  (Default root user for database)

* rake db:migrate

* rake db:seed

* tests run with rspec
