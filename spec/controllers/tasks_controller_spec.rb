require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  before do
    @task = FactoryGirl.create(:task)
  end

  describe "controller actions" do
    it "allows a task to be saved" do
       expect(@task.save).to be_truthy
    end

    it "allows a task to be deleted" do
      expect(@task.delete).to be_truthy
    end

    it "Allows updating" do
      put 'update', params: { id: @task.id, task: { title: 'Updated title'} }
      expect(@task.reload.title).to eq("Updated title")
    end

    it "Allows completing of task" do
      put 'complete', params: { id: @task.id, task: { completed: true } }
      expect(@task.reload.completed).to eq(true)
    end
  end
end
