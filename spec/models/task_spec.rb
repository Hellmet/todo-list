require 'rails_helper'

RSpec.describe Task, type: :model do
  before do
    @task = FactoryGirl.create(:task)
  end
  it "is valid with valid attributes" do
    expect(@task).to be_valid
  end
  it "is not valid without a title" do
    @task.title = nil
    expect(@task).to_not be_valid
  end
  it "is not valid without a description" do
    @task.description = nil
    expect(@task).to_not be_valid
  end
end
