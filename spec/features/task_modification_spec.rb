require 'spec_helper'
require 'capybara/rspec'

RSpec.feature "Task management", :type => :feature do

  scenario 'Create task and mark completed' do
    visit root_path
    click_link('new_task_link')
    fill_in 'Title', with: "Test title"
    fill_in 'Description', with: "Test description"
    click_button 'Create Task'

    expect(page).to have_content('Test title')

  end




end
