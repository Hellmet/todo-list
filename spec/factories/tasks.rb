FactoryGirl.define do
  factory :task do
  title 'Test title'
  description 'A test description'
  completed false
  end
end
