class TasksController < ApplicationController

  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)
    respond_to do |format|
      if @task.save
        format.html { redirect_to root_path, notice: "Task created successfully" }
        format.js
      else
        format.html { render :new, notice: @task.errors.full_messages }
      end
    end
  end

  def index
    @completed_tasks = Task.where(completed: true)
    @incomplete_tasks = Task.where(completed:false)
  end

  def show
    @task = Task.find(params[:id])
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    @task = Task.find(params[:id])
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to root_path, danger: "Task deleted successfully" }
        format.js
      else
        format.html { render :edit, notice: @task.errors.full_messages }
      end
    end
  end

  def complete
    @task = Task.find(params[:id])
    respond_to do |format|
      if @task.completed?
        @task.update_attributes(completed: false)
        format.html { redirect_to root_path }
        format.js
      else
        @task.update_attributes(completed: true)
        format.html { redirect_to root_path }
        format.js
      end
    end
  end

  def destroy
    @task = Task.find(params[:id]).destroy
    respond_to do |format|
      format.html { redirect_to root_path, danger: "Task deleted successfully" }
      format.js
    end
  end

  private

  def task_params
    params.require(:task).permit(:title, :description, :completed)
  end
end
