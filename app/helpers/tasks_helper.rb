module TasksHelper

  def completed_text(task)
    return "Mark incomplete" if task.completed
    "Mark completed"
  end
end
