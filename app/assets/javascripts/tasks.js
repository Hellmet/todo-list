jQuery.fn.submitOnCheck = function() {
  this.find('input[type=checkbox]').click(function() {
    $(this).parent('form').submit();
  });
  return this;
}

$( document ).on('turbolinks:load', function() {
  $('.edit_task').submitOnCheck();
  $('.comp-toggle').click(function() {
    $(".completed-tasks-list").toggle()
    $(".incomplete-tasks-list").toggle()
  });
});
